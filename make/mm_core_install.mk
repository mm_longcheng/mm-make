# install_model := mm_core_packet
# install_files := ../../../../mm_core/mm/proj/android/bin/mm_core.jar
# install_opath := $(NDK_APP_LIBS_OUT)
# include $(MAKE_HOME)/mm_core_install.mk

private_model := $(install_model)
private_files := $(install_files)
private_opath := $(install_opath)

private_objs := $(foreach n,$(private_files),$(private_opath)/$(call notdir,$(n:%/=%)))

private_files_rule := $(private_files:%=%.rule)
# private local variable.
$(private_files_rule): private_rule_files := $(private_files)
$(private_files_rule): private_rule_opath := $(private_opath)

$(private_files_rule):
	$(hide) $(call host-mkdir,$(private_rule_opath))
	$(hide) $(call host-install,$(@:%.rule=%),$(private_rule_opath)/$(call notdir,$(@:%.rule=%)))

$(private_objs):$(private_files_rule)

$(private_model):$(private_objs)
# $(info $(private_files_rule))
WANTED_INSTALLED_MODULES += $(private_model)