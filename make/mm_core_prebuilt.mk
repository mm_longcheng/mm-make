# prebuilt_model := mm_core_packet
# prebuilt_files := ../../../../mm_core/mm/proj/android/bin/mm_core.jar
# prebuilt_opath := $(NDK_APP_LIBS_OUT)
# include $(MAKE_HOME)/mm_core_prebuilt.mk

private_model := $(prebuilt_model)
private_files := $(prebuilt_files)
private_opath := $(prebuilt_opath)

private_objs := $(foreach n,$(private_files),$(private_opath)/$(call notdir,$(n:%/=%)))

private_files_rule := $(private_files:%=%.rule)
# private local variable.
$(private_files_rule): private_rule_files := $(private_files)
$(private_files_rule): private_rule_opath := $(private_opath)

$(private_files_rule):
	$(call host-echo-build-step,$(APP_ABI),Prebuilt) "$(call notdir,$(@:%.rule=%)) <= $(call pretty-dir,$(dir $(@)))"
	$(hide) $(call host-mkdir,$(private_rule_opath))
	$(hide) $(call host-cp,$(@:%.rule=%),$(private_rule_opath)/$(call notdir,$(@:%.rule=%)))

$(private_objs):$(private_files_rule)

$(private_model):$(private_objs)
#$(info $(private_files_rule))
WANTED_INSTALLED_MODULES += $(private_model)