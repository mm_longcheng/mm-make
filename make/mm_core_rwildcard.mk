#  
# config self source file path ,suffix.
# MY_FILES_PATH += $(LOCAL_PATH)/../../../src

# config filter out file and path.
# MY_FILTER_OUT += ../../proj.android%

# files suffix.
# MY_FILES_SUFFIX += %.cpp %.c %.cc
########################################################################
# traverse all file function.
rwildcard = $(wildcard $1$2) $(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2))

# get all source file.
MY_ALL_FILES := $(foreach src_path,$(MY_FILES_PATH), $(call rwildcard,$(src_path),*.*) ) 
MY_ALL_FILES := $(MY_ALL_FILES:$(MY_CPP_PATH)/./%=$(MY_CPP_PATH)%)
MY_SRC_LIST  := $(filter $(MY_FILES_SUFFIX),$(MY_ALL_FILES)) 
MY_SRC_LIST  := $(MY_SRC_LIST:$(LOCAL_PATH)/%=%)
MY_SRC_LIST  := $(filter-out $(MY_FILTER_OUT),$(MY_SRC_LIST)) 

# assign to NDK system.
LOCAL_SRC_FILES  += $(MY_SRC_LIST)

# $(info $(LOCAL_SRC_FILES))
# $(info $(LOCAL_C_INCLUDES))          
########################################################################