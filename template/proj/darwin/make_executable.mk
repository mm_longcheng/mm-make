LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)
########################################################################
LOCAL_MODULE := make_executable
LOCAL_MODULE_FILENAME := make_executable
########################################################################
LOCAL_CFLAGS += -fPIC
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += libmake_shared
########################################################################
LOCAL_STATIC_LIBRARIES += libmake_static
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../src
########################################################################
LOCAL_SRC_FILES  += ../../src/main.c
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
# MY_SOURCES_PATH += $(LOCAL_PATH)/../../src

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
include $(SOURCE_RWILDCARD)
########################################################################
include $(BUILD_EXECUTABLE)
########################################################################
# $(modules-dump-database)
