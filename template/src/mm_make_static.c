#include "mm_make_static.h"
#include <stdio.h>

MM_EXPORT_MAKE void mm_make_static_func(void)
{
	printf("%s %d\n", __FUNCTION__, __LINE__);
}