#include <stdio.h>

#include "mm_make_shared.h"
#include "mm_make_static.h"

int main(int argc,char **argv)
{	
	printf("%s %d\n", __FUNCTION__, __LINE__);
	mm_make_shared_func();
	mm_make_static_func();
	printf("%s %d\n", __FUNCTION__, __LINE__);
	return 0;
}