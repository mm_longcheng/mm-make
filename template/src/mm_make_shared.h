#ifndef __mm_make_shared_h__
#define __mm_make_shared_h__

#include "mm_export_make.h"

#include "mm_prefix.h"
/////////////////////////////////////////////////
MM_EXPORT_MAKE void mm_make_shared_func(void);
/////////////////////////////////////////////////
#include "mm_suffix.h"

#endif//__mm_make_shared_h__