#ifndef __mm_export_make_h__
#define __mm_export_make_h__

// #include "core/mmPlatform.h"
#define MM_PLATFORM_WIN32 1
#define MM_PLATFORM_LINUX 2

#ifdef _MSC_VER
#define MM_PLATFORM MM_PLATFORM_WIN32
#else
#define MM_PLATFORM MM_PLATFORM_LINUX
#endif//_MSC_VER

// windwos
#if MM_PLATFORM == MM_PLATFORM_WIN32

/* DLLExport define */
#  ifndef MM_EXPORT_MAKE
#    ifdef MM_SHARED_MAKE
#      ifdef MM_BUILD_MAKE
         /* We are building this library */
#        define MM_EXPORT_MAKE __declspec(dllexport)
#      else
         /* We are using this library */
#        define MM_EXPORT_MAKE __declspec(dllimport)
#      endif
#    else
#      define MM_EXPORT_MAKE
#    endif
#  endif

#  ifndef MM_PRIVATE_MAKE
#    define MM_PRIVATE_MAKE 
#  endif

#  ifndef MM_DEPRECATED_MAKE
#    define MM_DEPRECATED_MAKE __declspec(deprecated)
#  endif

#  ifndef MM_DEPRECATED_EXPORT_MAKE
#    define MM_DEPRECATED_EXPORT_MAKE MM_EXPORT_MAKE MM_DEPRECATED_MAKE
#  endif

#  ifndef MM_DEPRECATED_PRIVATE_MAKE
#    define MM_DEPRECATED_PRIVATE_MAKE MM_PRIVATE_MAKE MM_DEPRECATED_MAKE
#  endif

#else// unix

// Add -fvisibility=hidden to compiler options. With -fvisibility=hidden, you are telling
// GCC that every declaration not explicitly marked with a visibility attribute (MM_EXPORT)
// has a hidden visibility (like in windows).

/* DLLExport define */
#  ifndef MM_EXPORT_MAKE
#    ifdef MM_SHARED_MAKE
#      ifdef MM_BUILD_MAKE
         /* We are building this library */
#        define MM_EXPORT_MAKE __attribute__ ((visibility("default")))
#      else
         /* We are using this library */
#        define MM_EXPORT_MAKE
#      endif
#    else
#      define MM_EXPORT_MAKE
#    endif
#  endif

#  ifndef MM_PRIVATE_MAKE
#    define MM_PRIVATE_MAKE 
#  endif

#  ifndef MM_DEPRECATED_MAKE
#    define MM_DEPRECATED_MAKE __attribute__ ((deprecated))
#  endif

#  ifndef MM_DEPRECATED_EXPORT_MAKE
#    define MM_DEPRECATED_EXPORT_MAKE MM_EXPORT_MAKE MM_DEPRECATED_MAKE
#  endif

#  ifndef MM_DEPRECATED_PRIVATE_MAKE
#    define MM_DEPRECATED_PRIVATE_MAKE MM_PRIVATE_MAKE MM_DEPRECATED_MAKE
#  endif

#endif

#endif//__mm_export_make_h__