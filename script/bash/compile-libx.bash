#!/bin/bash

# Must define "arr" array.
#################################################
BuildProject()
{
	cd $1
	cd proj_${2}
	bash compile.bash ${3}
	cd ..
	cd ..
}
BuildProjectSequence()
{
	for i in $(seq $2 $3 $4)
	do
		# printf "%s\t%s %s %s\n" "$i" "${arr[$i]}" "$5" "$6"
		BuildProject ${arr[$i]} $5 $6
	done
}
BuildArray()
{
	local _action=$4
	local _max=`expr $2 - 1`
	if [[ ${_action} = "clean" ]] ; then
		BuildProjectSequence $1 ${_max} -1 0 $3 $4
	else
		BuildProjectSequence $1 0 +1 ${_max} $3 $4
	fi
}
BuildCompile()
{
	cd ../../build
	BuildArray $1 $2 $3 $4
	cd ../script/compile
}
#################################################
