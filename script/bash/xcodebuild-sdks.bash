ACTION=$@

compile_target()
{
	echo "> $1 \"$2\" $3 $4"
	xcodebuild ${ACTION} -project $1.xcodeproj -scheme "$2" -sdk $3 -configuration $4 | xcpretty
}

xcodebuild_sdks()
{
	echo "> xcodebuild sdks $1"
	compile_target $1 "compile"  "macosx"          "Debug"
	compile_target $1 "compile"  "macosx"          "Release"
	compile_target $1 "compile"  "iphoneos"        "Debug"
	compile_target $1 "compile"  "iphoneos"        "Release"
	compile_target $1 "compile"  "iphonesimulator" "Debug"
	compile_target $1 "compile"  "iphonesimulator" "Release"
}

xcodebuild_apps()
{
	echo "> xcodebuild apps $1"
	compile_target $1 "$1 macOS" "macosx"          "Debug"
	compile_target $1 "$1 macOS" "macosx"          "Release"
	compile_target $1 "$1 iOS"   "iphoneos"        "Debug"
	compile_target $1 "$1 iOS"   "iphoneos"        "Release"
	compile_target $1 "$1 iOS"   "iphonesimulator" "Debug"
	compile_target $1 "$1 iOS"   "iphonesimulator" "Release"
}

xcodebuild_csdk()
{
	echo "> xcodebuild csdk $1"
	compile_target $1 "compile macOS" "macosx"          "Debug"
	compile_target $1 "compile macOS" "macosx"          "Release"
	compile_target $1 "compile iOS"   "iphoneos"        "Debug"
	compile_target $1 "compile iOS"   "iphoneos"        "Release"
	compile_target $1 "compile iOS"   "iphonesimulator" "Debug"
	compile_target $1 "compile iOS"   "iphonesimulator" "Release"
}
