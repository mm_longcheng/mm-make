@echo off

set VSVARS="%VS141COMNTOOLS%\vcvars64.bat"
:: set VC_VER=141
call %VSVARS% 1>nul

call :BuildSolution %~1 %~2 %~3 %~4
GOTO :EOF

:BuildSolution
SETLOCAL
:: REM.
set _Directory=%~1
set _ABI=%~2
set _Name=%~3
set _ACTION=%~4
set _SlnUrl=%_Directory%%_Name%.sln
if "%_ACTION%"=="clean" (
	devenv %_SlnUrl% /Clean "Debug|%_ABI%"
	devenv %_SlnUrl% /Clean "Release|%_ABI%"
) else (
	devenv %_SlnUrl% /build "Debug|%_ABI%"
	devenv %_SlnUrl% /build "Release|%_ABI%"
)
ENDLOCAL
GOTO :EOF
