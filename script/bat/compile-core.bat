@echo off
setlocal enabledelayedexpansion

call :BuildCompile %*
GOTO :EOF

:: Must define "arr" array.
:: #################################################
:BuildProject
SETLOCAL
:: REM.
echo %~1 %~2 %~3
echo compile %~1
cd %~1
cd proj/%~2
@echo y | call compile.bat %~3
cd ..
cd ..
ENDLOCAL
GOTO :EOF

:BuildProjectSequence
SETLOCAL
:: REM.
set arr=%~1
set _a=%~2
set _b=%~3
set _c=%~4
for /l %%i in (%_a%,%_b%,%_c%) do (
	:: echo %%i !%arr%[%%i]! %~5 %~6
	call :BuildProject !%arr%[%%i]! %~5 %~6
)
ENDLOCAL
GOTO :EOF

:BuildArray
SETLOCAL
:: REM.
set _action=%~4
set /a _max=%~2-1
if "%_action%"=="clean" (
	call :BuildProjectSequence %~1 %_max% -1 0 %~3 %~4
) else (
	call :BuildProjectSequence %~1 0 +1 %_max% %~3 %~4
)
ENDLOCAL
GOTO :EOF

:BuildCompile
SETLOCAL
:: REM.
cd ../..
call :BuildArray %~1 %~2 %~3 %~4
cd script/compile
ENDLOCAL
GOTO :EOF