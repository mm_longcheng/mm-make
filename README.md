##mm-make is a common make file script.

add environment variable target to make path, if you need new make system.
MM_HOME=mm

add environment variable target to make path, if you need old make system.
MAKE_HOME=??/make

MM_MAKE_HOME target to this directory.
compile directory is new system from android ndk system.
i use for native toolchain for common make modules.

optional
compile-init-environment.mk is optional, will include at compile-init.mk.

most rule for make target is come from ndk.
bug, we have some different at compile process.
only for native.

template is project for how to use.

make <module>
make clean-<module>-<ARCH_ABI>

make
make clean
make install
make uninstall
