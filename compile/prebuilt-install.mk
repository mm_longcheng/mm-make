# INSTALL_MODEL := SOME_INSTALL_MODEL
# INSTALL_FILES := bin/some.jar
# INSTALL_OPATH := $(NDK_APP_BIN_OUT)
# include $(PREBUILT_INSTALL)

PRIVATE_MODEL := $(INSTALL_MODEL)
PRIVATE_FILES := $(INSTALL_FILES)
PRIVATE_OPATH := $(INSTALL_OPATH)

PRIVATE_FILES_RULE := $(PRIVATE_FILES:%=%.$(TARGET_ARCH_ABI).rule)

PRIVATE_OBJS := $(foreach n,$(PRIVATE_FILES),$(PRIVATE_OPATH)/$(call notdir,$(n:%/=%)))

# private local variable.
$(PRIVATE_FILES_RULE): PRIVATE_RULE_FILES := $(PRIVATE_FILES)
$(PRIVATE_FILES_RULE): PRIVATE_RULE_OPATH := $(PRIVATE_OPATH)
$(PRIVATE_FILES_RULE): PRIVATE_ABI := $(TARGET_ARCH_ABI)

$(PRIVATE_FILES_RULE):
	$(call host-echo-build-step,$(PRIVATE_ABI),Install) "$(call notdir,$(@:%.$(PRIVATE_ABI).rule=%)) <= $(call pretty-dir,$(@:%.$(PRIVATE_ABI).rule=%))"
	$(hide) $(call host-mkdir,$(PRIVATE_RULE_OPATH))
	$(hide) $(call host-install,$(@:%.$(PRIVATE_ABI).rule=%),$(PRIVATE_RULE_OPATH)/$(call notdir,$(@:%.$(PRIVATE_ABI).rule=%)))

$(PRIVATE_OBJS):$(PRIVATE_FILES_RULE)

$(PRIVATE_MODEL):$(PRIVATE_OBJS)
# $(info $(PRIVATE_FILES_RULE))
WANTED_INSTALLED_MODULES += $(PRIVATE_MODEL)