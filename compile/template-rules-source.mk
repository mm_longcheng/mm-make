# =============================================================================
#
# Build commands support
#
# =============================================================================

get-object-name = $(strip \
    $(subst ../,__/,\
      $(subst :,_,\
        $(eval __obj := $1)\
        $(foreach __ext,.c .s .S .asm $(LOCAL_CPP_EXTENSION) $(LOCAL_RS_EXTENSION),\
            $(eval __obj := $(__obj:%$(__ext)=%$(TARGET_OBJ_EXTENSION)))\
        )\
        $(__obj)\
    )))

-test-get-object-name = \
  $(eval TARGET_OBJ_EXTENSION=.o)\
  $(eval LOCAL_CPP_EXTENSION ?= .cpp)\
  $(eval LOCAL_RS_EXTENSION ?= .rs)\
  $(call test-expect,foo.o,$(call get-object-name,foo.c))\
  $(call test-expect,bar.o,$(call get-object-name,bar.s))\
  $(call test-expect,zoo.o,$(call get-object-name,zoo.S))\
  $(call test-expect,tot.o,$(call get-object-name,tot.cpp))\
  $(call test-expect,RS.o,$(call get-object-name,RS.rs))\
  $(call test-expect,goo.o,$(call get-object-name,goo.asm))

get-rs-scriptc-name = $(strip \
    $(subst ../,__/,\
      $(subst :,_,\
        $(eval __obj := $1)\
        $(foreach __ext,$(LOCAL_RS_EXTENSION),\
            $(eval __obj := $(__obj:%$(__ext)=%.cpp))\
        )\
        $(dir $(__obj))ScriptC_$(notdir $(__obj))\
    )))

get-rs-bc-name = $(strip \
    $(subst ../,__/,\
      $(subst :,_,\
        $(eval __obj := $1)\
        $(foreach __ext,$(LOCAL_RS_EXTENSION),\
            $(eval __obj := $(__obj:%$(__ext)=%.bc))\
        )\
        $(__obj)\
    )))

get-rs-so-name = $(strip \
    $(subst ../,__/,\
      $(subst :,_,\
        $(eval __obj := $1)\
        $(foreach __ext,$(LOCAL_RS_EXTENSION),\
            $(eval __obj := $(__obj:%$(__ext)=%$(TARGET_SONAME_EXTENSION)))\
        )\
        $(notdir $(__obj))\
    )))

# -----------------------------------------------------------------------------
# Macro    : hide
# Returns  : nothing
# Usage    : $(hide)<make commands>
# Rationale: To be used as a prefix for Make build commands to hide them
#            by default during the build. To show them, set V=1 in your
#            environment or command-line.
#
#            For example:
#
#                foo.o: foo.c
#                -->|$(hide) <build-commands>
#
#            Where '-->|' stands for a single tab character.
#
# -----------------------------------------------------------------------------
ifeq ($(V),1)
hide = $(empty)
else
hide = @
endif


# -----------------------------------------------------------------------------
# Function  : local-source-file-path
# Parameters: $1: source file (as listed in LOCAL_SRC_FILES)
# Returns   : full source file path of $1
# Usage     : $(call local-source-file-path,$1)
# Rationale : Used to compute the full path of a source listed in
#             LOCAL_SRC_FILES. If it is an absolute path, then this
#             returns the input, otherwise, prepends $(LOCAL_PATH)/
#             to the result.
# -----------------------------------------------------------------------------
local-source-file-path = $(if $(call host-path-is-absolute,$1),$1,$(LOCAL_PATH)/$1)

# This assumes that many variables have been pre-defined:
# _SRC: source file
# _OBJ: destination file
# _CC: 'compiler' command
# _FLAGS: 'compiler' flags
# _TEXT: Display text (e.g. "Compile++ thumb", must be EXACTLY 15 chars long)
#
define ev-build-file
$$(_OBJ): PRIVATE_ABI      := $$(TARGET_ARCH_ABI)
$$(_OBJ): PRIVATE_SRC      := $$(_SRC)
$$(_OBJ): PRIVATE_OBJ      := $$(_OBJ)
$$(_OBJ): PRIVATE_DEPS     := $$(call host-path,$$(_OBJ).d)
$$(_OBJ): PRIVATE_MODULE   := $$(LOCAL_MODULE)
$$(_OBJ): PRIVATE_TEXT     := $$(_TEXT)
$$(_OBJ): PRIVATE_CC       := $$(_CC)
$$(_OBJ): PRIVATE_CFLAGS   := $$(_FLAGS)

ifeq ($$(LOCAL_SHORT_COMMANDS),true)
_OPTIONS_LISTFILE := $$(_OBJ).cflags
$$(_OBJ): $$(call generate-list-file,$$(_FLAGS),$$(_OPTIONS_LISTFILE))
$$(_OBJ): PRIVATE_CFLAGS := @$$(call host-path,$$(_OPTIONS_LISTFILE))
$$(_OBJ): $$(_OPTIONS_LISTFILE)
endif

$$(call generate-file-dir,$$(_OBJ))
$$(_OBJ): $$(_SRC) $$(LOCAL_MAKEFILE) $$(NDK_APP_APPLICATION_MK) $(LOCAL_RS_OBJECTS)
	$$(call host-echo-build-step,$$(PRIVATE_ABI),$$(PRIVATE_TEXT)) "$$(PRIVATE_MODULE) <= $$(notdir $$(PRIVATE_SRC))"
	$$(hide) $$(PRIVATE_CC) -MMD -MP -MF $$(PRIVATE_DEPS) $$(PRIVATE_CFLAGS) $$(call host-path,$$(PRIVATE_SRC)) -o $$(call host-path,$$(PRIVATE_OBJ))
endef


# For renderscript: slightly different from the above ev-build-file
# _RS_SRC: RS source file
# _CPP_SRC: ScriptC_RS.cpp source file
# _BC_SRC: Bitcode source file
# _BC_SO: Bitcode SO name, no path
# _OBJ: destination file
# _RS_CC: 'compiler' command for _RS_SRC
# _RS_BCC: 'compiler' command for _BC_SRC
# _CXX: 'compiler' command for _CPP_SRC
# _RS_FLAGS: 'compiler' flags for _RS_SRC
# _CPP_FLAGS: 'compiler' flags for _CPP_SRC
# _LD_FLAGS: 'compiler' flags for linking
# _TEXT: Display text (e.g. "Compile RS")
# _OUT: output dir
# _COMPAT: 'true' if bcc_compat is required
#
define ev-build-rs-file
$$(_OBJ): PRIVATE_ABI       := $$(TARGET_ARCH_ABI)
$$(_OBJ): PRIVATE_RS_SRC    := $$(_RS_SRC)
$$(_OBJ): PRIVATE_CPP_SRC   := $$(_CPP_SRC)
$$(_OBJ): PRIVATE_BC_SRC    := $$(_BC_SRC)
$$(_OBJ): PRIVATE_OBJ       := $$(_OBJ)
$$(_OBJ): PRIVATE_BC_OBJ    := $$(_BC_SRC)$(TARGET_OBJ_EXTENSION)
$$(_OBJ): PRIVATE_BC_SO     := $$(_BC_SO)
$$(_OBJ): PRIVATE_DEPS      := $$(call host-path,$$(_OBJ).d)
$$(_OBJ): PRIVATE_MODULE    := $$(LOCAL_MODULE)
$$(_OBJ): PRIVATE_TEXT      := $$(_TEXT)
$$(_OBJ): PRIVATE_RS_CC     := $$(_RS_CC)
$$(_OBJ): PRIVATE_RS_BCC    := $$(_RS_BCC)
$$(_OBJ): PRIVATE_CXX       := $$(_CXX)
$$(_OBJ): PRIVATE_RS_FLAGS  := $$(_RS_FLAGS)
$$(_OBJ): PRIVATE_CPPFLAGS  := $$(_CPP_FLAGS)
$$(_OBJ): PRIVATE_LD        := $$(TARGET_LD)
$$(_OBJ): PRIVATE_LDFLAGS   := $$(_LD_FLAGS)
$$(_OBJ): PRIVATE_OUT       := $$(TARGET_OUT)
$$(_OBJ): PRIVATE_RS_TRIPLE := $$(RS_TRIPLE)
$$(_OBJ): PRIVATE_COMPAT    := $$(_COMPAT)
$$(_OBJ): PRIVATE_LIB_PATH  := $$(RENDERSCRIPT_TOOLCHAIN_PREBUILT_ROOT)/platform/$(TARGET_ARCH)

ifeq ($$(LOCAL_SHORT_COMMANDS),true)
_OPTIONS_LISTFILE := $$(_OBJ).cflags
$$(_OBJ): $$(call generate-list-file,$$(_CPP_FLAGS),$$(_OPTIONS_LISTFILE))
$$(_OBJ): PRIVATE_CPPFLAGS := @$$(call host-path,$$(_OPTIONS_LISTFILE))
$$(_OBJ): $$(_OPTIONS_LISTFILE)
endif

# x86_64 & mips64 has both lib/ and lib64/, use lib64 for 64bit RenderScript compilation.
ifneq ($(filter x86_64 mips64,$(TARGET_ARCH_ABI)),)
$$(_OBJ): PRIVATE_SYS_PATH := $$(call host-path,$(SYSROOT_LINK)/usr/lib64)
else
$$(_OBJ): PRIVATE_SYS_PATH := $$(call host-path,$(SYSROOT_LINK)/usr/lib)
endif

# llvm-rc-cc.exe has problem accepting input *.rs with path. To workaround:
# cd ($dir $(_SRC)) ; llvm-rs-cc $(notdir $(_SRC)) -o ...full-path...
#
ifeq ($$(_COMPAT),true)
	# In COMPAT mode, use LD instead of CXX to bypass the gradle check for their book-keeping of native libs.
	# And this is what we do with SDK.
	# TODO: We could use CXX after gradle can correctly handle librs.*.so.
$$(_OBJ): $$(_RS_SRC) $$(LOCAL_MAKEFILE) $$(NDK_APP_APPLICATION_MK)
	$$(call host-echo-build-step,$$(PRIVATE_ABI),$$(PRIVATE_TEXT)) "$$(PRIVATE_MODULE) <= $$(notdir $$(PRIVATE_RS_SRC))"
	$$(hide) \
	cd $$(call host-path,$$(dir $$(PRIVATE_RS_SRC))) && $$(PRIVATE_RS_CC) -o $$(call host-path,$$(abspath $$(dir $$(PRIVATE_OBJ))))/ -d $$(abspath $$(call host-path,$$(dir $$(PRIVATE_OBJ)))) -MD -reflect-c++ -target-api $(strip $(subst android-,,$(APP_PLATFORM))) $$(PRIVATE_RS_FLAGS) $$(notdir $$(PRIVATE_RS_SRC))
	$$(hide) \
	$$(PRIVATE_RS_BCC) -O3 -o $$(call host-path,$$(PRIVATE_BC_OBJ)) -fPIC -shared -rt-path $$(PRIVATE_LIB_PATH)/librsrt.bc -mtriple $$(PRIVATE_RS_TRIPLE) $$(call host-path,$$(PRIVATE_BC_SRC)) && \
	$$(PRIVATE_LD) -shared -Bsymbolic -z noexecstack -z relro -z now -nostdlib $$(call host-path,$$(PRIVATE_BC_OBJ)) $$(PRIVATE_LIB_PATH)/libcompiler_rt.a -o $$(call host-path,$$(PRIVATE_OUT)/librs.$$(PRIVATE_BC_SO)) -L $$(PRIVATE_SYS_PATH) -L $$(PRIVATE_LIB_PATH) -lRSSupport -lm -lc && \
	$$(PRIVATE_CXX) -MMD -MP -MF $$(PRIVATE_DEPS) $$(PRIVATE_CPPFLAGS) $$(call host-path,$$(PRIVATE_CPP_SRC)) -o $$(call host-path,$$(PRIVATE_OBJ))
else
$$(_OBJ): $$(_RS_SRC) $$(LOCAL_MAKEFILE) $$(NDK_APP_APPLICATION_MK)
	$$(call host-echo-build-step,$$(PRIVATE_ABI),$$(PRIVATE_TEXT)) "$$(PRIVATE_MODULE) <= $$(notdir $$(PRIVATE_RS_SRC))"
	$$(hide) \
	cd $$(call host-path,$$(dir $$(PRIVATE_RS_SRC))) && $$(PRIVATE_RS_CC) -o $$(call host-path,$$(abspath $$(dir $$(PRIVATE_OBJ))))/ -d $$(abspath $$(call host-path,$$(dir $$(PRIVATE_OBJ)))) -MD -reflect-c++ -target-api $(strip $(subst android-,,$(APP_PLATFORM))) $$(PRIVATE_RS_FLAGS) $$(notdir $$(PRIVATE_RS_SRC))
	$$(hide) \
	$$(PRIVATE_CXX) -MMD -MP -MF $$(PRIVATE_DEPS) $$(PRIVATE_CPPFLAGS) $$(call host-path,$$(PRIVATE_CPP_SRC)) -o $$(call host-path,$$(PRIVATE_OBJ))
endif
endef

# This assumes the same things than ev-build-file, but will handle
# the definition of LOCAL_FILTER_ASM as well.
define ev-build-source-file
LOCAL_DEPENDENCY_DIRS += $$(dir $$(_OBJ))
ifndef LOCAL_FILTER_ASM
  # Trivial case: Directly generate an object file
  $$(eval $$(call ev-build-file))
else
  # This is where things get hairy, we first transform
  # the source into an assembler file, send it to the
  # filter, then generate a final object file from it.
  #

  # First, remember the original settings and compute
  # the location of our temporary files.
  #
  _ORG_SRC := $$(_SRC)
  _ORG_OBJ := $$(_OBJ)
  _ORG_FLAGS := $$(_FLAGS)
  _ORG_TEXT  := $$(_TEXT)

  _OBJ_ASM_ORIGINAL := $$(patsubst %$$(TARGET_OBJ_EXTENSION),%.s,$$(_ORG_OBJ))
  _OBJ_ASM_FILTERED := $$(patsubst %$$(TARGET_OBJ_EXTENSION),%.filtered.s,$$(_ORG_OBJ))

  # If the source file is a plain assembler file, we're going to
  # use it directly in our filter.
  ifneq (,$$(filter %.s,$$(_SRC)))
    _OBJ_ASM_ORIGINAL := $$(_SRC)
  endif

  #$$(info SRC=$$(_SRC) OBJ=$$(_OBJ) OBJ_ORIGINAL=$$(_OBJ_ASM_ORIGINAL) OBJ_FILTERED=$$(_OBJ_ASM_FILTERED))

  # We need to transform the source into an assembly file, instead of
  # an object. The proper way to do that depends on the file extension.
  #
  # For C and C++ source files, simply replace the -c by an -S in the
  # compilation command (this forces the compiler to generate an
  # assembly file).
  #
  # For assembler templates (which end in .S), replace the -c with -E
  # to send it to the preprocessor instead.
  #
  # Don't do anything for plain assembly files (which end in .s)
  #
  ifeq (,$$(filter %.s,$$(_SRC)))
    _OBJ   := $$(_OBJ_ASM_ORIGINAL)
    ifneq (,$$(filter %.S,$$(_SRC)))
      _FLAGS := $$(patsubst -c,-E,$$(_ORG_FLAGS))
    else
      _FLAGS := $$(patsubst -c,-S,$$(_ORG_FLAGS))
    endif
    $$(eval $$(call ev-build-file))
  endif

  # Next, process the assembly file with the filter
  $$(_OBJ_ASM_FILTERED): PRIVATE_ABI    := $$(TARGET_ARCH_ABI)
  $$(_OBJ_ASM_FILTERED): PRIVATE_SRC    := $$(_OBJ_ASM_ORIGINAL)
  $$(_OBJ_ASM_FILTERED): PRIVATE_DST    := $$(_OBJ_ASM_FILTERED)
  $$(_OBJ_ASM_FILTERED): PRIVATE_FILTER := $$(LOCAL_FILTER_ASM)
  $$(_OBJ_ASM_FILTERED): PRIVATE_MODULE := $$(LOCAL_MODULE)
  $$(_OBJ_ASM_FILTERED): $$(_OBJ_ASM_ORIGINAL)
	$$(call host-echo-build-step,$$(PRIVATE_ABI),AsmFilter) "$$(PRIVATE_MODULE) <= $$(notdir $$(PRIVATE_SRC))"
	$$(hide) $$(PRIVATE_FILTER) $$(PRIVATE_SRC) $$(PRIVATE_DST)

  # Then, generate the final object, we need to keep assembler-specific
  # flags which look like -Wa,<option>:
  _SRC   := $$(_OBJ_ASM_FILTERED)
  _OBJ   := $$(_ORG_OBJ)
  _FLAGS := $$(filter -Wa%,$$(_ORG_FLAGS)) -c
  _TEXT  := Assembly
  $$(eval $$(call ev-build-file))
endif
endef

# -----------------------------------------------------------------------------
# Template  : ev-compile-c-source
# Arguments : 1: single C source file name (relative to LOCAL_PATH)
#             2: target object file (without path)
# Returns   : None
# Usage     : $(eval $(call ev-compile-c-source,<srcfile>,<objfile>)
# Rationale : Internal template evaluated by compile-c-source and
#             compile-s-source
# -----------------------------------------------------------------------------
define  ev-compile-c-source
_SRC:=$$(call local-source-file-path,$(1))
_OBJ:=$$(LOCAL_OBJS_DIR:%/=%)/$(2)

_FLAGS := $$($$(my)CFLAGS) \
          $$(call get-src-file-target-cflags,$(1)) \
          $$(call host-c-includes,$$(LOCAL_C_INCLUDES) $$(LOCAL_PATH)) \
          $$(NDK_APP_CFLAGS) \
          $$(NDK_APP_CONLYFLAGS) \
          $$(LOCAL_CFLAGS) \
          $$(LOCAL_CONLYFLAGS) \
          --sysroot $$(call host-path,$$(SYSROOT_INC)) \
          $(SYSROOT_ARCH_INC_ARG) \
          -c \

_TEXT := Compile $$(call get-src-file-text,$1)
_CC   := $$(NDK_CCACHE) $$(TARGET_CC)

$$(eval $$(call ev-build-source-file))
endef

# -----------------------------------------------------------------------------
# Template  : ev-compile-s-source
# Arguments : 1: single .S source file name (relative to LOCAL_PATH)
#             2: target object file (without path)
# Returns   : None
# Usage     : $(eval $(call ev-compile-s-source,<srcfile>,<objfile>)
# -----------------------------------------------------------------------------
define  ev-compile-s-source
_SRC:=$$(call local-source-file-path,$(1))
_OBJ:=$$(LOCAL_OBJS_DIR:%/=%)/$(2)

_FLAGS := $$($$(my)CFLAGS) \
          $$(call get-src-file-target-cflags,$(1)) \
          $$(call host-c-includes,$$(LOCAL_C_INCLUDES) $$(LOCAL_PATH)) \
          $$(NDK_APP_CFLAGS) \
          $$(NDK_APP_ASFLAGS) \
          $$(LOCAL_CFLAGS) \
          $$(LOCAL_ASFLAGS) \
          -isystem $$(call host-path,$$(SYSROOT_INC)/usr/include) \
          -c \

_TEXT := Compile $$(call get-src-file-text,$1)
_CC   := $$(NDK_CCACHE) $$(TARGET_CC)

$$(eval $$(call ev-build-source-file))
endef

# -----------------------------------------------------------------------------
# Template  : ev-compile-asm-source
# Arguments : 1: single ASM source file name (relative to LOCAL_PATH)
#             2: target object file (without path)
# Returns   : None
# Usage     : $(eval $(call ev-compile-asm-source,<srcfile>,<objfile>)
# Rationale : Internal template evaluated by compile-asm-source
# -----------------------------------------------------------------------------
define  ev-compile-asm-source
_SRC:=$$(call local-source-file-path,$(1))
_OBJ:=$$(LOCAL_OBJS_DIR:%/=%)/$(2)

_FLAGS := $$(call host-c-includes,$$(LOCAL_C_INCLUDES) $$(LOCAL_PATH)) \
          $$(call get-src-file-target-cflags,$(1)) \
          $$(LOCAL_ASMFLAGS) \
          $$(NDK_APP_ASMFLAGS) \
          -I $$(call host-path,$$(SYSROOT_INC)/usr/include) \
          $(subst -isystem,-I,$(SYSROOT_ARCH_INC_ARG)) \
          $$(if $$(filter x86_64, $$(TARGET_ARCH_ABI)), -f elf64, -f elf32 -m x86)

_TEXT := Assemble $$(call get-src-file-text,$1)
_CC   := $$(NDK_CCACHE) $$(TARGET_ASM)

$$(_OBJ): PRIVATE_ABI      := $$(TARGET_ARCH_ABI)
$$(_OBJ): PRIVATE_SRC      := $$(_SRC)
$$(_OBJ): PRIVATE_OBJ      := $$(_OBJ)
$$(_OBJ): PRIVATE_MODULE   := $$(LOCAL_MODULE)
$$(_OBJ): PRIVATE_TEXT     := $$(_TEXT)
$$(_OBJ): PRIVATE_CC       := $$(_CC)
$$(_OBJ): PRIVATE_CFLAGS   := $$(_FLAGS)

ifeq ($$(LOCAL_SHORT_COMMANDS),true)
_OPTIONS_LISTFILE := $$(_OBJ).cflags
$$(_OBJ): $$(call generate-list-file,$$(_FLAGS),$$(_OPTIONS_LISTFILE))
$$(_OBJ): PRIVATE_CFLAGS := @$$(call host-path,$$(_OPTIONS_LISTFILE))
$$(_OBJ): $$(_OPTIONS_LISTFILE)
endif

$$(call generate-file-dir,$$(_OBJ))
$$(_OBJ): $$(_SRC) $$(LOCAL_MAKEFILE) $$(NDK_APP_APPLICATION_MK) $(LOCAL_RS_OBJECTS)
	$$(call host-echo-build-step,$$(PRIVATE_ABI),$$(PRIVATE_TEXT)) "$$(PRIVATE_MODULE) <= $$(notdir $$(PRIVATE_SRC))"
	$$(hide) $$(PRIVATE_CC) $$(PRIVATE_CFLAGS) $$(call host-path,$$(PRIVATE_SRC)) -o $$(call host-path,$$(PRIVATE_OBJ))
endef

# -----------------------------------------------------------------------------
# Function  : compile-c-source
# Arguments : 1: single C source file name (relative to LOCAL_PATH)
#             2: object file
# Returns   : None
# Usage     : $(call compile-c-source,<srcfile>,<objfile>)
# Rationale : Setup everything required to build a single C source file
# -----------------------------------------------------------------------------
compile-c-source = $(eval $(call ev-compile-c-source,$1,$2))

# -----------------------------------------------------------------------------
# Function  : compile-s-source
# Arguments : 1: single Assembly source file name (relative to LOCAL_PATH)
#             2: object file
# Returns   : None
# Usage     : $(call compile-s-source,<srcfile>,<objfile>)
# Rationale : Setup everything required to build a single Assembly source file
# -----------------------------------------------------------------------------
compile-s-source = $(eval $(call ev-compile-s-source,$1,$2))

# -----------------------------------------------------------------------------
# Function  : compile-asm-source
# Arguments : 1: single Assembly source file name (relative to LOCAL_PATH)
#             2: object file
# Returns   : None
# Usage     : $(call compile-asm-source,<srcfile>,<objfile>)
# Rationale : Setup everything required to build a single Assembly source file
# -----------------------------------------------------------------------------
compile-asm-source = $(eval $(call ev-compile-asm-source,$1,$2))

# -----------------------------------------------------------------------------
# Template  : ev-compile-cpp-source
# Arguments : 1: single C++ source file name (relative to LOCAL_PATH)
#             2: target object file (without path)
# Returns   : None
# Usage     : $(eval $(call ev-compile-cpp-source,<srcfile>,<objfile>)
# Rationale : Internal template evaluated by compile-cpp-source
# -----------------------------------------------------------------------------

define  ev-compile-cpp-source
_SRC:=$$(call local-source-file-path,$(1))
_OBJ:=$$(LOCAL_OBJS_DIR:%/=%)/$(2)
_FLAGS := $$($$(my)CXXFLAGS) \
          $$(call get-src-file-target-cflags,$(1)) \
          $$(call host-c-includes, $$(LOCAL_C_INCLUDES) $$(LOCAL_PATH)) \
          $(STL_DEFAULT_STD_VERSION) \
          $$(NDK_APP_CFLAGS) \
          $$(NDK_APP_CPPFLAGS) \
          $$(NDK_APP_CXXFLAGS) \
          $$(LOCAL_CFLAGS) \
          $$(LOCAL_CPPFLAGS) \
          $$(LOCAL_CXXFLAGS) \
          --sysroot $$(call host-path,$$(SYSROOT_INC)) \
          $(SYSROOT_ARCH_INC_ARG) \
          -c \

_CC   := $$(NDK_CCACHE) $$($$(my)CXX)
_TEXT := Compile++ $$(call get-src-file-text,$1)

$$(eval $$(call ev-build-source-file))
endef

# -----------------------------------------------------------------------------
# Function  : compile-cpp-source
# Arguments : 1: single C++ source file name (relative to LOCAL_PATH)
#           : 2: object file name
# Returns   : None
# Usage     : $(call compile-cpp-source,<srcfile>)
# Rationale : Setup everything required to build a single C++ source file
# -----------------------------------------------------------------------------
compile-cpp-source = $(eval $(call ev-compile-cpp-source,$1,$2))

# -----------------------------------------------------------------------------
# Template  : ev-compile-rs-source
# Arguments : 1: single RS source file name (relative to LOCAL_PATH)
#             2: intermediate cpp file (without path)
#             3: intermediate bc file (without path)
#             4: so file from bc (without path)
#             5: target object file (without path)
#             6: 'true' if bcc_compat is required
# Returns   : None
# Usage     : $(eval $(call ev-compile-rs-source,<srcfile>,<cppfile>,<objfile>)
# Rationale : Internal template evaluated by compile-rs-source
# -----------------------------------------------------------------------------

define  ev-compile-rs-source
_RS_SRC:=$$(call local-source-file-path,$(1))
_CPP_SRC:=$$(LOCAL_OBJS_DIR:%/=%)/$(2)
_BC_SRC:=$$(LOCAL_OBJS_DIR:%/=%)/$(3)
_BC_SO:=$(4)
_OBJ:=$$(LOCAL_OBJS_DIR:%/=%)/$(5)
_COMPAT := $(6)
_CPP_FLAGS := $$($$(my)CXXFLAGS) \
          $$(call get-src-file-target-cflags,$(1)) \
          $$(call host-c-includes, $$(LOCAL_C_INCLUDES) $$(LOCAL_PATH)) \
          $$(NDK_APP_CFLAGS) \
          $$(NDK_APP_CPPFLAGS) \
          $$(NDK_APP_CXXFLAGS) \
          $$(LOCAL_CFLAGS) \
          $$(LOCAL_CPPFLAGS) \
          $$(LOCAL_CXXFLAGS) \
          --sysroot $$(call host-path,$$(SYSROOT_INC)) \
          $(SYSROOT_ARCH_INC_ARG) \
          -fno-rtti \
          -c \

_LD_FLAGS := $$(TARGET_LDFLAGS)

_RS_FLAGS := $$(call host-c-includes, $$(LOCAL_RENDERSCRIPT_INCLUDES) $$(LOCAL_PATH)) \
          $$($$(my)RS_FLAGS) \
          $$(LOCAL_RENDERSCRIPT_FLAGS) \
          $$(call host-c-includes,$$($(my)RENDERSCRIPT_INCLUDES)) \

_RS_CC  := $$(NDK_CCACHE) $$($$(my)RS_CC)
_RS_BCC := $$(NDK_CCACHE) $$($$(my)RS_BCC)
_CXX    := $$(NDK_CCACHE) $$($$(my)CXX)
_TEXT   := Compile RS
_OUT    := $$($$(my)OUT)

$$(eval $$(call ev-build-rs-file))
endef

# -----------------------------------------------------------------------------
# Function  : compile-rs-source
# Arguments : 1: single RS source file name (relative to LOCAL_PATH)
#             2: intermediate cpp file name
#             3: intermediate bc file
#             4: so file from bc (without path)
#             5: object file name
#             6: 'true' if bcc_compat is required
# Returns   : None
# Usage     : $(call compile-rs-source,<srcfile>)
# Rationale : Setup everything required to build a single RS source file
# -----------------------------------------------------------------------------
compile-rs-source = $(eval $(call ev-compile-rs-source,$1,$2,$3,$4,$5,$6))