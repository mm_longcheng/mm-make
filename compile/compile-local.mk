NDK_APPLICATION_MK := $(NDK_ROOT)/compile/default-application.mk
NDK_APP_OUT := $(strip $(NDK_OUT))
NDK_APP_LIBS_OUT := $(strip $(NDK_LIBS_OUT))

# Fake an application named 'local'
_app            := local
_application_mk := $(NDK_APPLICATION_MK)
NDK_APPS        := $(_app)

# include $(BUILD_SYSTEM)/add-application.mk

include $(BUILD_SYSTEM)/setup-imports.mk

include $(BUILD_SYSTEM)/compile-all.mk