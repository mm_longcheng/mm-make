#
MM_MAKE_OBJ_DIR ?= obj
MM_MAKE_BIN_DIR ?= bin
MM_MAKE_DST_DIR ?= install
MM_MAKE_TARGET_PLATFORM ?= linux

# use for target platform special path.
MM_PLATFORM     ?= $(MM_MAKE_TARGET_PLATFORM)

#
NDK_ROOT         = $(MM_MAKE_HOME)
NDK_OUT          = $(MM_MAKE_OBJ_DIR)
NDK_LIBS_OUT     = $(MM_MAKE_BIN_DIR)
NDK_APP_DST_DIR  = $(MM_MAKE_DST_DIR)
NDK_TOOLCHAIN    = x86_64-gcc-native
NDK_APP_ABI      = x86_64
NDK_PROJECT_PATH = .
NDK_PLATFORMS_ROOT    = $(NDK_ROOT)/platforms
NDK_TOOLCHAIN_VERSION = gcc

NDK_ALL_PLATFORMS += $(MM_MAKE_TARGET_PLATFORM)

# application abi
APP_PLATFORM := $(MM_MAKE_TARGET_PLATFORM)

# TARGET_ARCH = x86_64
# TARGET_ABIS = x86_64

# toolchain
TARGET_PLATFORM = $(APP_PLATFORM)
TARGET_ARCH_ABI = x86_64
TARGET_OUT      = $(NDK_LIBS_OUT)
TARGET_OBJS     = $(NDK_OUT)
TARGET_ABI      = $(TARGET_PLATFORM)-$(TARGET_ARCH_ABI)

TARGET_CC  := gcc
TARGET_CXX := g++
TARGET_ASM := yasm
TARGET_LD  := ld
TARGET_AR  := ar

TARGET_LIBGCC    := 
TARGET_LIBATOMIC := 

TARGET_STRIP    := strip
TARGET_OBJCOPY  := objcopy

TARGET_ARFLAGS := -cr

TARGET_OBJ_EXTENSION := .o
TARGET_LIB_EXTENSION := .a
TARGET_SONAME_EXTENSION := .so

SYSROOT_BASE := /
SYSROOT_INC := $(SYSROOT_BASE)
SYSROOT_LINK := $(SYSROOT_BASE)
SYSROOT_ARCH_INC_ARG := 
#######################################
TARGET_NO_UNDEFINED_LDFLAGS := 

TARGET_NO_EXECUTE_CFLAGS  := -Wa,--noexecstack
TARGET_NO_EXECUTE_LDFLAGS := 

TARGET_DISABLE_NO_EXECUTE_CFLAGS  := -Wa,--execstack
TARGET_DISABLE_NO_EXECUTE_LDFLAGS := 

TARGET_RELRO_LDFLAGS := 

TARGET_DISABLE_RELRO_LDFLAGS := 

TARGET_FORMAT_STRING_CFLAGS := -Wformat -Werror=format-security

TARGET_DISABLE_FORMAT_STRING_CFLAGS := -Wno-error=format-security

TARGET_BUILD_ID_LDFLAGS := 

TARGET_WARN_SHARED_TEXTREL_LDFLAGS := 

TARGET_FATAL_WARNINGS_LDFLAGS :=
#######################################
cmd-strip ?= $(PRIVATE_STRIP) $(call host-path,$1)
#######################################
# expand-cflag for shared library.
ifeq ($(cmd-build-shared-library-expand-cflag),)
define cmd-build-shared-library-expand-cflag
    -shared \
    --sysroot=$(call host-path,$(PRIVATE_SYSROOT_LINK))
endef
endif

# expand-cflag for executable.
ifeq ($(cmd-build-executable-expand-cflag),)
define cmd-build-executable-expand-cflag
    --sysroot=$(call host-path,$(PRIVATE_SYSROOT_LINK))
endef
endif
#######################################
COMPILE_INIT                := $(MM_MAKE_HOME)/compile/compile-init.mk
COMPILE_LOCAL               := $(MM_MAKE_HOME)/compile/compile-local.mk
#######################################
