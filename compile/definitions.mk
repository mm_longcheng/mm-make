# Copyright (C) 2009 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Common definitions for the Android NDK build system
#

# We use the GNU Make Standard Library
include $(NDK_ROOT)/compile/gmsl/gmsl

include $(BUILD_SYSTEM)/definitions-tests.mk
include $(BUILD_SYSTEM)/definitions-utils.mk
include $(BUILD_SYSTEM)/definitions-host.mk
include $(BUILD_SYSTEM)/definitions-graph.mk

include $(BUILD_SYSTEM)/definitions-tools.mk
include $(BUILD_SYSTEM)/definitions-modules.mk
include $(BUILD_SYSTEM)/definitions-application.mk
include $(BUILD_SYSTEM)/definitions-target-cflags.mk
include $(BUILD_SYSTEM)/definitions-func.mk

include $(BUILD_SYSTEM)/template-rules-source.mk
include $(BUILD_SYSTEM)/template-rules-target.mk

include $(BUILD_SYSTEM)/definitions-stl.mk

include $(BUILD_SYSTEM)/definitions-sources.mk
include $(BUILD_SYSTEM)/definitions-my.mk
