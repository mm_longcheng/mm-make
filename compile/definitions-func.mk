# Copyright (C) 2009 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Common definitions for the Android NDK build system
#

# -----------------------------------------------------------------------------
# Function : explain-file-content
# Returns  : the file content.
# Usage    : $(call explain-file-content, "filename.txt")
# -----------------------------------------------------------------------------
explain-file-content=$(shell if [ -f $1 ]; then cat $1; else echo ""; fi)

# -----------------------------------------------------------------------------
# Function : explain-ar-at-flags
# Returns  : explain ar @ flags.
# Usage    : $(call explain-ar-at-flags, $(ARFLAGS))
# -----------------------------------------------------------------------------
ifeq ($(HOST_OS),darwin)
explain-ar-at-flags=$(foreach _flag,$1, $(call explain-file-content,$(_flag:@%=%)))
else
explain-ar-at-flags=@$1
endif
