#
# C++ STL support
#

# The list of registered STL implementations we support
NDK_STL_LIST :=

# Used internally to register a given STL implementation, see below.
#
# $1: STL name as it appears in APP_STL (e.g. system)
# $2: STL module name (e.g. cxx-stl/system)
# $3: list of static libraries all modules will depend on
# $4: list of shared libraries all modules will depend on
# $5: list of ldlibs to be exported to all modules
# $6: Default standard version for this STL (with `-std` prefix).
#
ndk-stl-register = \
    $(eval __ndk_stl := $(strip $1)) \
    $(eval NDK_STL_LIST += $(__ndk_stl)) \
    $(eval NDK_STL.$(__ndk_stl).IMPORT_MODULE := $(strip $2)) \
    $(eval NDK_STL.$(__ndk_stl).STATIC_LIBS := $(strip $(call strip-lib-prefix,$3))) \
    $(eval NDK_STL.$(__ndk_stl).SHARED_LIBS := $(strip $(call strip-lib-prefix,$4))) \
    $(eval NDK_STL.$(__ndk_stl).EXPORT_LDLIBS := $(strip $5)) \
    $(eval NDK_STL.$(__ndk_stl).DEFAULT_STD_VERSION := $(strip $6))

# Called to check that the value of APP_STL is a valid one.
# $1: STL name as it apperas in APP_STL (e.g. 'system')
#
ndk-stl-check = \
    $(if $(call set_is_member,$(NDK_STL_LIST),$1),,\
        $(call __ndk_info,Invalid APP_STL value: $1)\
        $(call __ndk_info,Please use one of the following instead: $(NDK_STL_LIST))\
        $(call __ndk_error,Aborting))

# Called before the top-level Android.mk is parsed to
# select the STL implementation.
# $1: STL name as it appears in APP_STL (e.g. system)
#
ndk-stl-select = \
    $(call import-module,$(NDK_STL.$1.IMPORT_MODULE)) \
    $(eval STL_DEFAULT_STD_VERSION := $(strip $(NDK_STL.$1.DEFAULT_STD_VERSION)))

# Called after all Android.mk files are parsed to add
# proper STL dependencies to every C++ module.
# $1: STL name as it appears in APP_STL (e.g. system)
#
ndk-stl-add-dependencies = \
    $(call modules-add-c++-dependencies,\
        $(NDK_STL.$1.STATIC_LIBS),\
        $(NDK_STL.$1.SHARED_LIBS),\
        $(NDK_STL.$1.LDLIBS))

#
#

# Register the 'system' STL implementation
#
$(call ndk-stl-register,\
    system,\
    cxx-stl/system,\
    libstdc++,\
    ,\
    \
    )

# Register the 'stlport_static' STL implementation
#
$(call ndk-stl-register,\
    stlport_static,\
    cxx-stl/stlport,\
    stlport_static,\
    ,\
    \
    )

# Register the 'stlport_shared' STL implementation
#
$(call ndk-stl-register,\
    stlport_shared,\
    cxx-stl/stlport,\
    ,\
    stlport_shared,\
    \
    )

# Register the 'gnustl_static' STL implementation
#
$(call ndk-stl-register,\
    gnustl_static,\
    cxx-stl/gnu-libstdc++,\
    gnustl_static,\
    ,\
    \
    )

# Register the 'gnustl_shared' STL implementation
#
$(call ndk-stl-register,\
    gnustl_shared,\
    cxx-stl/gnu-libstdc++,\
    ,\
    gnustl_shared,\
    \
    )

# Register the 'c++_static' STL implementation
#
$(call ndk-stl-register,\
    c++_static,\
    cxx-stl/llvm-libc++,\
    c++_static libc++abi android_support,\
    ,\
    -ldl,\
    -std=c++11\
    )

# Register the 'c++_shared' STL implementation
#
$(call ndk-stl-register,\
    c++_shared,\
    cxx-stl/llvm-libc++,\
    libandroid_support,\
    c++_shared,\
    ,\
    -std=c++11\
    )

# The 'none' APP_STL value corresponds to no C++ support at
# all. Used by some of the STLport and GAbi++ test projects.
#
$(call ndk-stl-register,\
    none,\
    cxx-stl/system,\
    )

ifneq (,$(NDK_UNIT_TESTS))
$(call ndk-run-all-tests)
endif
