# The list of default c extensions supported by GCC.
default-ccc-extensions := .c

# The list of default asm extensions supported by GCC
default-asm-extensions := .s .S

# -----------------------------------------------------------------------------
# Function : rwildcard
# Arguments: 1: directory path
#            2: pattern (*.mk *.c *.cc *.cpp *.cxx *.h *.o *.d)
# Returns  : a list of all files immediately below some directory
# Usage    : $(call rwildcard, <some path>, <pattern>)
# -----------------------------------------------------------------------------
rwildcard=$(wildcard $1$2) $(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2))

# -----------------------------------------------------------------------------
# Macro    : local-makefile
# Returns  : the name of the last parsed Android.mk file
# Usage    : $(local-makefile)
# -----------------------------------------------------------------------------
local-makefile = $(if $(LOCAL_MAKEFILE),$(LOCAL_MAKEFILE),$(lastword $(filter %Android.mk,$(MAKEFILE_LIST))))

# -----------------------------------------------------------------------------
# Function  : compile-f-cpp-source
# Arguments : 1: single Assembly source file name (relative to LOCAL_PATH)
#             2: object file
# Returns   : None
# Usage     : $(call compile-ccc-source,<srcfile>,<objfile>)
# Rationale : Setup everything required to build a single C++ source file
# -----------------------------------------------------------------------------
compile-f-cpp-source = $(eval $(call ev-compile-c++-source,$1,$2))

# -----------------------------------------------------------------------------
# Function  : compile-f-ccc-source
# Arguments : 1: single Assembly source file name (relative to LOCAL_PATH)
#             2: object file
# Returns   : None
# Usage     : $(call compile-ccc-source,<srcfile>,<objfile>)
# Rationale : Setup everything required to build a single c source file
# -----------------------------------------------------------------------------
compile-f-ccc-source = $(eval $(call ev-compile-c-source,$1,$2))

# -----------------------------------------------------------------------------
# Function  : compile-f-asm-source
# Arguments : 1: single Assembly source file name (relative to LOCAL_PATH)
#             2: object file
# Returns   : None
# Usage     : $(call compile-asm-source,<srcfile>,<objfile>)
# Rationale : Setup everything required to build a single Assembly source file
# -----------------------------------------------------------------------------
compile-f-asm-source = $(eval $(call ev-compile-s-source,$1,$2))
