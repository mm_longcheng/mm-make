# =============================================================================
#
# Application.mk support
#
# =============================================================================

# the list of variables that *must* be defined in Application.mk files
NDK_APP_VARS_REQUIRED :=

# the list of variables that *may* be defined in Application.mk files
NDK_APP_VARS_OPTIONAL := \
    APP_ABI \
    APP_ASFLAGS \
    APP_ASMFLAGS \
    APP_BUILD_SCRIPT \
    APP_CFLAGS \
    APP_CONLYFLAGS \
    APP_CPPFLAGS \
    APP_CXXFLAGS \
    APP_LDFLAGS \
    APP_MODULES \
    APP_OPTIM \
    APP_PIE \
    APP_PLATFORM \
    APP_PROJECT_PATH \
    APP_SHORT_COMMANDS \
    APP_STL \
    APP_THIN_ARCHIVE \

# the list of all variables that may appear in an Application.mk file
# or defined by the build scripts.
NDK_APP_VARS := \
    $(NDK_APP_VARS_REQUIRED) \
    $(NDK_APP_VARS_OPTIONAL) \
    APP_DEBUG \
    APP_DEBUGGABLE \
    APP_MANIFEST \
