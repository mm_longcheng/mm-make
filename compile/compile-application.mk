_map := NDK_APP.$(_app)
######################################################################
# strip the 'lib' prefix in front of APP_MODULES modules
APP_MODULES := $(call strip-lib-prefix,$(APP_MODULES))

APP_PROJECT_PATH := $(strip $(APP_PROJECT_PATH))
ifndef APP_PROJECT_PATH
    APP_PROJECT_PATH := $(NDK_PROJECT_PATH)
endif

# If APP_PIE isn't defined, set it to true for android-$(NDK_FIRST_PIE_PLATFORM_LEVEL) and above
#
# APP_PIE := $(strip $(APP_PIE))
# $(call ndk_log,  APP_PIE is $(APP_PIE))
# ifndef APP_PIE
#     ifneq (,$(call gte,$(APP_PLATFORM_LEVEL),$(NDK_FIRST_PIE_PLATFORM_LEVEL)))
#         APP_PIE := true
#         $(call ndk_log,  Enabling -fPIE)
#     else
#         APP_PIE := false
#     endif
# endif

# Check that the value of APP_ABI corresponds to known ABIs
# 'all' is a special case that means 'all supported ABIs'
#
# It will be handled in setup-app.mk. We can't hope to change
# the value of APP_ABI is the user enforces it on the command-line
# with a call like:  ndk-build APP_ABI=all
#
# Because GNU Make makes the APP_ABI variable read-only (any assignments
# to it will be ignored)
#
APP_ABI := $(subst $(comma),$(space),$(strip $(APP_ABI)))
ifndef APP_ABI
    APP_ABI := $(NDK_DEFAULT_ABIS)
endif

# Determine whether the application should be debuggable.
# - If APP_DEBUG is set to 'true', then it always should.
# - If APP_DEBUG is set to 'false', then it never should
# - Otherwise, extract the android:debuggable attribute from the manifest.
#
ifdef APP_DEBUG
  APP_DEBUGGABLE := $(APP_DEBUG)
  ifeq ($(NDK_LOG),1)
    ifeq ($(APP_DEBUG),true)
      $(call ndk_log,Application '$(_app)' forced debuggable through NDK_DEBUG)
    else
      $(call ndk_log,Application '$(_app)' forced *not* debuggable through NDK_DEBUG)
    endif
  endif
else
  # NOTE: To make unit-testing simpler, handle the case where there is no manifest.
  APP_DEBUGGABLE := false
  ifdef APP_MANIFEST
    APP_DEBUGGABLE := $(shell $(HOST_PYTHON) $(BUILD_PY)/extract_manifest.py debuggable $(call host-path,$(APP_MANIFEST)))
  endif
  ifeq ($(NDK_LOG),1)
    ifeq ($(APP_DEBUGGABLE),true)
      $(call ndk_log,Application '$(_app)' *is* debuggable)
    else
      $(call ndk_log,Application '$(_app)' is not debuggable)
    endif
  endif
endif

# LOCAL_BUILD_MODE will be either release or debug
#
# If APP_OPTIM is defined in the Application.mk, just use this.
#
# Otherwise, set to 'debug' if android:debuggable is set to TRUE,
# and to 'release' if not.
#
ifneq ($(APP_OPTIM),)
    # check that APP_OPTIM, if defined, is either 'release' or 'debug'
    $(if $(filter-out release debug,$(APP_OPTIM)),\
        $(call __ndk_info, The APP_OPTIM defined in $(_application_mk) must only be 'release' or 'debug')\
        $(call __ndk_error,Aborting)\
    )
    $(call ndk_log,Selecting optimization mode through Application.mk: $(APP_OPTIM))
else
    ifeq ($(APP_DEBUGGABLE),true)
        $(call ndk_log,Selecting debug optimization mode (app is debuggable))
        APP_OPTIM := debug
    else
        $(call ndk_log,Selecting release optimization mode (app is not debuggable))
        APP_OPTIM := release
    endif
endif

APP_CFLAGS := $(strip $(APP_CFLAGS))
APP_CONLYFLAGS := $(strip $(APP_CONLYFLAGS))
APP_CPPFLAGS := $(strip $(APP_CPPFLAGS))
APP_CXXFLAGS := $(strip $(APP_CXXFLAGS))
APP_RENDERSCRIPT_FLAGS := $(strip $(APP_RENDERSCRIPT_FLAGS))
APP_ASFLAGS := $(strip $(APP_ASFLAGS))
APP_ASMFLAGS := $(strip $(APP_ASMFLAGS))
APP_LDFLAGS  := $(strip $(APP_LDFLAGS))

# Check that APP_STL is defined. If not, use the default value (system)
# otherwise, check that the name is correct.
APP_STL := $(strip $(APP_STL))
ifndef APP_STL
    APP_STL := system
else
    $(call ndk-stl-check,$(APP_STL))
endif

$(if $(call get,$(_map),defined),\
  $(call __ndk_info,Weird, the application $(_app) is already defined by $(call get,$(_map),defined))\
  $(call __ndk_error,Aborting)\
)

$(call set,$(_map),defined,$(_application_mk))

# Record all app-specific variable definitions
$(foreach __name,$(NDK_APP_VARS),\
  $(call set,$(_map),$(__name),$($(__name)))\
)

# Record the Application.mk for debugging
$(call set,$(_map),Application.mk,$(_application_mk))

NDK_ALL_APPS += $(_app)
######################################################################
# ok, let's parse all Android.mk source files in order to build
# the modules for this app.
#

# Restore the APP_XXX variables just for this pass as NDK_APP_XXX
#
NDK_APP_NAME           := $(_app)
NDK_APP_APPLICATION_MK := $(call get,$(_map),Application.mk)

$(foreach __name,$(NDK_APP_VARS),\
  $(eval NDK_$(__name) := $(call get,$(_map),$(__name)))\
)

# make the application depend on the modules it requires
.PHONY: ndk-app-$(_app)
ndk-app-$(_app): $(NDK_APP_MODULES)
all: ndk-app-$(_app)
######################################################################
# recompute all dependencies between modules
$(call modules-compute-dependencies)

# Now compute the closure of all module dependencies.
#
# If APP_MODULES is not defined in the Application.mk, we
# will build all modules that were listed from the top-level Android.mk
# and the installable imported ones they depend on
#
ifeq ($(strip $(NDK_APP_MODULES)),)
    WANTED_MODULES := $(call modules-get-all-installable,$(modules-get-top-list))
    ifeq (,$(strip $(WANTED_MODULES)))
        WANTED_MODULES := $(modules-get-top-list)
        $(call ndk_log,[$(TARGET_ARCH_ABI)] No installable modules in project - forcing static library build)
    endif
else
    WANTED_MODULES := $(call module-get-all-dependencies,$(NDK_APP_MODULES))
endif

$(call ndk_log,[$(TARGET_ARCH_ABI)] Modules to build: $(WANTED_MODULES))

WANTED_INSTALLED_MODULES += $(call map,module-get-installed,$(WANTED_MODULES))
######################################################################
# for debugging purpose
ifdef NDK_DEBUG_MODULES
$(call modules-dump-database)
endif

# compile modules binary macro.
compile-modules-binary = $(foreach __pass2_module,$(__ndk_modules),\
    $(eval LOCAL_MODULE := $(__pass2_module))\
    $(eval include $(BUILD_SYSTEM)/compile-binary.mk)\
)

# now, really build the modules, the second pass allows one to deal
# with exported values
$(compile-modules-binary)
