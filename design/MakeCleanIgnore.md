make clean ignore the PREBUILT_*

```
include $(CLEAR_VARS)
LOCAL_MODULE := foo
LOCAL_SRC_FILES := bar.so
LOCAL_EXPORT_C_INCLUDES := bar/location
# Avoid including on clean
ifneq ($(MAKECMDGOALS),clean)
    include $(PREBUILT_SHARED_LIBRARY)
endif
```