android ndk -std=c++98

note: use 'c++98' for 'ISO C++ 1998 with amendments' standard
note: use 'c++03' for 'ISO C++ 1998 with amendments' standard
note: use 'gnu++98' for 'ISO C++ 1998 with amendments and GNU extensions' standard
note: use 'c++0x' for 'ISO C++ 2011 with amendments' standard
note: use 'c++11' for 'ISO C++ 2011 with amendments' standard
note: use 'gnu++0x' for 'ISO C++ 2011 with amendments and GNU extensions' standard
note: use 'gnu++11' for 'ISO C++ 2011 with amendments and GNU extensions' standard
note: use 'c++1y' for 'ISO C++ 2014 with amendments' standard
note: use 'c++14' for 'ISO C++ 2014 with amendments' standard
note: use 'gnu++1y' for 'ISO C++ 2014 with amendments and GNU extensions' standard
note: use 'gnu++14' for 'ISO C++ 2014 with amendments and GNU extensions' standard
note: use 'c++1z' for 'Working draft for ISO C++ 2017' standard
note: use 'gnu++1z' for 'Working draft for ISO C++ 2017 with GNU extensions' standard
note: use 'cuda' for 'NVIDIA CUDA(tm)' standard